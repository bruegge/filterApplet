package de.mpg.mis.filterApplet;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FilterISBNVorstudie extends Filter
{

  public FilterISBNVorstudie(String linkerString,String rechterString)
  {
    super(linkerString,rechterString);
  }

  @Override
  protected LinkedList<LinkedList<String>> zerlegLinks(String string)
  {
    return zerlegRechts(string);
    /*
    Pattern pat=Pattern.compile(".*?[\\d\\-xX][\\d\\-xX]+[^\\d\\-xX]");
    Matcher mat=pat.matcher(string+"\n");
    LinkedHashMap<String,LinkedList<String>> map=new LinkedHashMap<String,LinkedList<String>>();
    int start=0;
    while(mat.find())
    {
      String isbn=string.substring(start,mat.end());
      start=mat.end();
      String norm=normiereRechts(isbn);
      LinkedList<String> list=map.get(norm);
      if(list==null)
      {
        list=new LinkedList<String>();
        map.put(norm,list);
      }
      list.add(isbn);
    }
    LinkedList<LinkedList<String>> aus=new LinkedList<LinkedList<String>>();
    for(String key:map.keySet())
    {
      aus.add(map.get(key));
    }
    return aus;
    */
  }

  @Override
  protected LinkedList<LinkedList<String>> zerlegRechts(String string)
  {
//    return zerlegLinks(string);
    
    Pattern pat=Pattern.compile("(\\s*\\n+\\s*)|(\\s+a)");
    Matcher mat=pat.matcher(string+"\n");
    LinkedHashMap<String,LinkedList<String>> map=new LinkedHashMap<String,LinkedList<String>>();
    int start=0;
    while(mat.find())
    {
      String group=mat.group();
      String isbn=string.substring(start,mat.start()); 
      start=mat.end();
      String norm=normiereRechts(isbn);
      LinkedList<String> list=map.get(norm);
      if(list==null)
      {
        list=new LinkedList<String>();
        map.put(norm,list);
      }
      list.add(isbn+group);
    }
    LinkedList<LinkedList<String>> aus=new LinkedList<LinkedList<String>>();
    for(String key:map.keySet())
    {
      aus.add(map.get(key));
    }
    return aus;
    
  }

  @Override
  protected String normiereLinks(String string)
  {
    String norm=string.replaceAll("[^\\dxX]","").toUpperCase();
    return norm;
  }

  @Override
  protected String normiereRechts(String string)
  {
    return normiereLinks(string);
  }

}
