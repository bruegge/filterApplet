package de.mpg.mis.filterApplet;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FilterWS extends Filter
{

  public FilterWS(String linkerString,String rechterString)
  {
    super(linkerString,rechterString);
  }

  protected LinkedList<LinkedList<String>> zerleg(String string,Pattern pat)
  {
    LinkedList<LinkedList<String>> aus=new LinkedList<>();
    Matcher mat=pat.matcher(string);
    while(mat.find())
    {
      LinkedList<String> l=new LinkedList<>();
      aus.add(l);
      l.add(mat.group());
    }
    return aus;
  }

  @Override
  protected LinkedList<LinkedList<String>> zerlegLinks(String string)
  {
//    return zerleg(string,Pattern.compile("978\\d+(?=;)"));
    return zerleg(string,Pattern.compile("978\\d+;"));
  }

  @Override
  protected LinkedList<LinkedList<String>> zerlegRechts(String string)
  {
//    return zerleg(string,Pattern.compile("(?<=a)978\\d+"));
    return zerleg(string,Pattern.compile("a978\\d+"));
  }

  @Override
  protected String normiereLinks(String string)
  {
    return string.replaceAll("[^\\d]","");
  }

  @Override
  protected String normiereRechts(String string)
  {
    return normiereLinks(string);
  }

}
