package de.mpg.mis.filterApplet;

import java.util.LinkedList;
import java.util.ListIterator;

public abstract class Filter
{
  protected String links;
  protected String rechts;
  protected LinkedList<LinkedList<String>> linksAlt;
  protected LinkedList<LinkedList<String>> rechtsAlt;
  protected LinkedList<LinkedList<String>> linksDa;
  protected LinkedList<LinkedList<String>> rechtsDa;

  public Filter(String linkerString,String rechterString)
  {
    links=linkerString;
    rechts=rechterString;
    linksAlt=zerlegLinks(linkerString);
    rechtsAlt=zerlegRechts(rechterString);
  }
  
  protected abstract LinkedList<LinkedList<String>> zerlegLinks(String string);

  protected abstract LinkedList<LinkedList<String>> zerlegRechts(String string);
  
  protected abstract String normiereLinks(String string);

  protected abstract String normiereRechts(String string);
  
  public void getIntersection()
  {
    linksDa=new LinkedList<LinkedList<String>>();
    rechtsDa=new LinkedList<LinkedList<String>>();
    ListIterator<LinkedList<String>> linksIterator=linksAlt.listIterator();
    while(linksIterator.hasNext())
    {
      LinkedList<String> linksZwi=linksIterator.next();
      String links=normiereLinks(linksZwi.getFirst());
      ListIterator<LinkedList<String>> rechtsIterator=rechtsAlt.listIterator();
      while(rechtsIterator.hasNext())
      {
        LinkedList<String> rechtsZwi=rechtsIterator.next();
        String rechts=normiereRechts(rechtsZwi.getFirst());
        if(rechts.equals(links))
        {
          linksDa.add(linksZwi);
          rechtsDa.add(rechtsZwi);
          linksIterator.remove();
          rechtsIterator.remove();
        }
      }
    }
  }

}

/*
Charlotte
Karla
Charlotte
Camilla
Anna
Carlotta
Anne

Charlotte,Charlotte,Carlotta
Karla
Anna,Anne



978-1-2345-6789-0
9781234567890
1234567890
345678686574
*/