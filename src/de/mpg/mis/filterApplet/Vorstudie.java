package de.mpg.mis.filterApplet;

import java.awt.GridLayout;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

public class Vorstudie
{
  public Vorstudie()
  {
  }
  
  public static class Tatenlauscher implements ActionListener
  {
    public JTextPane[] editors;
    
    public Tatenlauscher(JTextPane[] eds)
    {
      editors=eds;
    }
    public void actionPerformed(ActionEvent e)
    {
      String action=e.getActionCommand();
      if(action.equals("filter"))
      {
//        Filter filter=new FilterISBNVorstudie(editors[0].getText(),editors[1].getText());
        Filter filter=new FilterWS(editors[0].getText(),editors[1].getText());
        filter.getIntersection();
        editors[0].setText(filter.linksAlt.toString());
        editors[1].setText(filter.rechtsAlt.toString());
        editors[2].setText(filter.linksDa.toString());
        editors[3].setText(filter.rechtsDa.toString());
      }
    }
    
  }

  public static void main(String[] args) throws Exception
  {
    JFrame frame=new JFrame();   
    frame.setLocation(0,0);
    frame.setSize(1000,1000);
    frame.setVisible(true);
    GridLayout gridLayout=new GridLayout(2,2);
    frame.getContentPane().setLayout(gridLayout);
    JTextPane[] editors={new JTextPane(),new JTextPane(),new JTextPane(),new JTextPane()}; 
    for(JTextPane editor:editors)
    {
      frame.getContentPane().add(new JScrollPane(editor));
    }
    JMenuBar bar=new JMenuBar();
    frame.setJMenuBar(bar);
    JMenu menu=new JMenu("File"); 
    bar.add(menu);
    JMenuItem load=new JMenuItem("load");
    menu.add(load);
    JMenuItem save=new JMenuItem("save");
    menu.add(save);
    JMenu menuDo=new JMenu("Do"); 
    bar.add(menuDo);
    JMenuItem filter=new JMenuItem("filter");
    menuDo.add(filter);
    //fillText(editors);
    fillFile(editors);
    frame.pack();
    frame.getContentPane().repaint();
    filter.addActionListener(new Tatenlauscher(editors));
  }
  
  protected static void fillFile(JTextPane[] panes) throws Exception
  {
    String[] files={"data/ws.csv","data/ws.mrc"};
    int c=0;
    for(String file:files)
    {
      panes[c++].setText(readFile(file));
    }
  }
  
  protected static String readFile(String file) throws Exception
  {
    System.out.println(file);
    FileReader fr=new FileReader(file);
    StringBuilder aus=new StringBuilder();
    int c=0;
    while((c=fr.read())>0)
    {
      aus.append((char)c);
    }
    fr.close();
    System.out.println(" .. gelesen");
    return aus.toString();
  }
  
  protected static void fillText(JTextPane[] panes)
  {
    String string=" \na3764342226  3764342226 \na3764342226  0817642226 \na978-3-0348-7597-4 (e-isbn) 376436582X \na978-3-0348-7597-4 (e-isbn) 9783034875998 \na978-3-0348-7597-4 (e-isbn) 9783034875974 \na3-7643-4287-0 3764342870 \na3-7643-4287-0 0817642870 \na3-540-97279-X  354097279X \na3-540-97279-X  9781475769005 \na3-540-97279-X  9781475769785 \na3-540-97279-X  038797279X \na0-7354-0051-2  0735400547 \na0-7354-0051-2  0735400512 \na978-3-540-42813-8  9783540428138 \na978-3-540-42813-8  3540428135 \na2868835473  2868835473 \na2868835473  3540429786 "; 
    Pattern pat=Pattern.compile("[\\dxX]+\\s*\\n");
    Matcher mat=pat.matcher(string);
    StringBuilder sb1=new StringBuilder();
    StringBuilder sb2=new StringBuilder();
    while(mat.find())
    {
      String group=mat.group();
      String vor=string.substring(0,mat.start());
      string=string.substring(mat.end(),string.length());
      mat=pat.matcher(string);
      sb1.append(vor);
      sb2.append(group);
    }
    panes[0].setText(sb1.toString());
    panes[1].setText(sb2.toString());
  }
  
}
