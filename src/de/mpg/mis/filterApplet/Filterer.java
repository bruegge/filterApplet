package de.mpg.mis.filterApplet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public class Filterer
{

  public Filterer()
  {
  }
  
  public static void main(String[] args) throws Exception
  {
    String links=readFile("data/ws.csv");
    String rechts=readFile("data/ws.mrc");
    Filter filter=new FilterWS(links,rechts);
    filter.getIntersection();
    String[] names={"wsLA.txt","wsRA.txt","wsL.txt","wsR.txt"};
    String[] strings={filter.linksAlt.toString(),filter.rechtsAlt.toString(),filter.linksDa.toString(),filter.rechtsDa.toString()};
    int c=0;
    for(String name:names)
    {
      writeFile("data/"+name,strings[c++]);
    }
  }
  
  public static void writeFile(String fileName,String text) throws Exception
  {
    FileWriter fw=new FileWriter(fileName);
    fw.write(text);
    fw.close();
  }
  
  public static String readFile(String fileName) throws Exception
  {
    InputStreamReader fis=new InputStreamReader(new FileInputStream(new File(fileName)),Charset.forName("utf8"));
    StringBuilder aus=new StringBuilder();
    int c=0;
    while((c=fis.read())>0)
    {
      aus.append((char)c);
    }
    fis.close();
    return aus.toString();
  }

}
